var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var indexRouter = require("./routes/index");
var recipeRouter = require("./routes/recipes");

var app = express();

// view engine setup
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/recipes", recipeRouter);

app.use(function (err, req, res, next) {
  console.log(err);

  if (err.status === 400) res.status(400).json({ message: "NOT_FOUND" });
  else res.status(500).json({ message: "Something is wrong" });
});

module.exports = app;
