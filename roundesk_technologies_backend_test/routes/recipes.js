var recipes = require("../recipes.json");
var router = require("express").Router();

router.get("/recipes/:id", function (req, res) {
  recipes.findById({ _id: req.params.id }).exec(function (err, Request) {
    if (err) {
      res.json({ message: "NOT FOUND", status: 400, data: null });
    } else {
      res.json({ message: "Get recipe by id", status: 200, data: Recipe });
    }
  });
});

router.get("/recipes/step/:id", function (req, res) {
  recipes.findOne(
    { _id: req.params.id },
    {
      elapsedTime: req.body.elapsedTime,
    },
    function (err, Step) {
      if (err) {
        res.json({ message: "NOT FOUND", status: 400, data: null });
      } else {
        res.json({ message: "Get Step", status: 200, data: Step });
      }
    }
  );
});

module.exports = router;
