import React, { useState, useEffect } from "react";
import "./App.css";
import "h8k-components";

import Articles from "./components/Articles";

const title = "Sorting Articles";

function App({ articles }) {
  const [sortedArticles, setSortedArticles] = useState(articles);

  useEffect(() => {
    console.log("sorted articles after change in app.js", sortedArticles);
  }, [sortedArticles]);

  const sortArticlesByUpvotes = (e) => {
    e.preventDefault();
    const array = articles;
    array.sort((a, b) => b.upvotes - a.upvotes);
    setSortedArticles(array);
  };
  const sortArticlesByDate = (e) => {
    e.preventDefault();
    const array = articles;
    array.sort(function (a, b) {
      return new Date(b.date) - new Date(a.date);
    });
    setSortedArticles(array);
  };
  return (
    <div className="App">
      <h8k-navbar header={title}></h8k-navbar>
      <div className="layout-row align-items-center justify-content-center my-20 navigation">
        <label className="form-hint mb-0 text-uppercase font-weight-light">
          Sort By
        </label>
        <button
          data-testid="most-upvoted-link"
          className="small"
          onClick={sortArticlesByUpvotes}
        >
          Most Upvoted
        </button>
        <button
          data-testid="most-recent-link"
          className="small"
          onClick={sortArticlesByDate}
        >
          Most Recent
        </button>
      </div>

      <Articles articles={sortedArticles} />
    </div>
  );
}

export default App;
